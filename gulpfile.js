const gulp = require('gulp');
const ts = require('gulp-typescript');
const less = require('gulp-less');
const sourcemaps = require('gulp-sourcemaps');

gulp.task('build', function() {
  const merge = require('merge2');
  const tsProject = ts.createProject('tsconfig.json');
  const tsResult = tsProject.src().pipe(sourcemaps.init()).pipe(tsProject());
  const lessProject = gulp.src('src/styles/index.less').pipe(less());

  return merge([
    tsResult.dts,
    tsResult.js
    .pipe(sourcemaps.mapSources())
    .pipe(sourcemaps.write('./')),
    lessProject
  ])
  .pipe(gulp.dest('dist'));
});