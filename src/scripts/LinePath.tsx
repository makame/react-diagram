import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { Line } from './Line';

export class LinePath extends React.Component<{
    from: {
        x: number
        y: number
        side?: 'in' | 'out'
        offset?: number
        // rect?: {
        //     x: number
        //     y: number
        //     width: number
        //     height: number
        // }
    }
    to: {
        x: number
        y: number
        side?: 'in' | 'out'
        offset?: number
        // rect?: {
        //     x: number
        //     y: number
        //     width: number
        //     height: number
        // }
    }
    style?: string
    className?: string
}, {}> {

    lines: {
        start: {
            x: number
            y: number
        }
        end: {
            x: number
            y: number
        }
    }[] = [];

    lineIntersect(lineOne: {
        start: {
            x: number
            y: number
        }
        end: {
            x: number
            y: number
        }
    }, lineTwo: {
        start: {
            x: number
            y: number
        }
        end: {
            x: number
            y: number
        }
    }): boolean {
        var x = ((lineOne.start.x * lineOne.end.y - lineOne.start.y * lineOne.end.x) * (lineTwo.start.x - lineTwo.end.x) - (lineOne.start.x - lineOne.end.x) * (lineTwo.start.x * lineTwo.end.y - lineTwo.start.y * lineTwo.end.x)) / ((lineOne.start.x - lineOne.end.x) * (lineTwo.start.y - lineTwo.end.y) - (lineOne.start.y - lineOne.end.y) * (lineTwo.start.x - lineTwo.end.x));

        var y = ((lineOne.start.x * lineOne.end.y - lineOne.start.y * lineOne.end.x) * (lineTwo.start.y - lineTwo.end.y) - (lineOne.start.y - lineOne.end.y) * (lineTwo.start.x * lineTwo.end.y - lineTwo.start.y * lineTwo.end.x)) / ((lineOne.start.x - lineOne.end.x) * (lineTwo.start.y - lineTwo.end.y) - (lineOne.start.y - lineOne.end.y) * (lineTwo.start.x - lineTwo.end.x));

        if (isNaN(x) || isNaN(y)) {
            return false;
        }
        else {
            if (lineOne.start.x >= lineOne.end.x) {
                if (!(lineOne.end.x <= x && x <= lineOne.start.x)) return false;
            }
            else {
                if (!(lineOne.start.x <= x && x <= lineOne.end.x)) return false;
            }
            if (lineOne.start.y >= lineOne.end.y) {
                if (!(lineOne.end.y <= y && y <= lineOne.start.y)) return false;
            }
            else {
                if (!(lineOne.start.y <= y && y <= lineOne.end.y)) return false;
            }
            if (lineTwo.start.x >= lineTwo.end.x) {
                if (!(lineTwo.end.x <= x && x <= lineTwo.start.x)) return false;
            }
            else {
                if (!(lineTwo.start.x <= x && x <= lineTwo.end.x)) return false;
            }
            if (lineTwo.start.y >= lineTwo.end.y) {
                if (!(lineTwo.end.y <= y && y <= lineTwo.start.y)) return false;
            }
            else {
                if (!(lineTwo.start.y <= y && y <= lineTwo.end.y)) return false;
            }
        }

        return true;
    }

    pointsInRect(points: {
        x: number
        y: number
    }[], start: {
        x: number
        y: number
    }, end: {
        x: number
        y: number
    }): boolean {
        let startX = end.x > start.x ? start.x : end.x;
        let startY = end.y > start.y ? start.y : end.y;
        let endX = end.x > start.x ? end.x : start.x;
        let endY = end.y > start.y ? end.y : start.y;

        let contains = false;
        points.forEach(point => {
            if (point.x >= startX && point.x <= endX && point.y >= startY && point.y <= endY) contains = true;
        });
        return contains;
    }

    crossedRect(start: {
        x: number
        y: number
    }, end: {
        x: number
        y: number
    }): boolean {
        let startX = end.x > start.x ? start.x : end.x;
        let startY = end.y > start.y ? start.y : end.y;
        let endX = end.x > start.x ? end.x : start.x;
        let endY = end.y > start.y ? end.y : start.y;

        if (!this.lines) {
            this.lines = this.getPath();
        }

        let contains = false;
        this.lines.forEach(line => {
            if (!contains) contains = this.pointsInRect([ line.start, line.end ], start, end);
            if (!contains) contains = this.lineIntersect(line, { start, end });
        });
        return contains;
    }

    getPath(): {
        start: {
            x: number
            y: number
        }
        end: {
            x: number
            y: number
        }
    }[] {
        let lines = [];

        let from = {
            x: this.props.from.x,
            y: this.props.from.y
        }

        let to = {
            x: this.props.to.x,
            y: this.props.to.y
        }

        if (this.props.from.side && this.props.from.offset) {
            let sideX = 0;
            let sideY = 0;

            if (this.props.from.side == 'in') {
                sideX = from.x - this.props.from.offset;
                sideY = from.y;
            }
            else if (this.props.from.side == 'out') {
                sideX = from.x + this.props.from.offset;
                sideY = from.y;
            }

            lines.push({
                start: {
                    x: from.x,
                    y: from.y
                },
                end: {
                    x: sideX,
                    y: sideY
                }
            });

            from.x = sideX;
            from.y = sideY;
        }

        if (this.props.to.side && this.props.to.offset) {
            let sideX = 0;
            let sideY = 0;

            if (this.props.to.side == 'in') {
                sideX = to.x - this.props.to.offset;
                sideY = to.y;
            }
            else if (this.props.to.side == 'out') {
                sideX = to.x + this.props.to.offset;
                sideY = to.y;
            }

            lines.push({
                start: {
                    x: sideX,
                    y: sideY
                },
                end: {
                    x: to.x,
                    y: to.y
                }
            });

            to.x = sideX;
            to.y = sideY;
        }

        lines.push({
            start: {
                x: from.x,
                y: from.y
            },
            end: {
                x: from.x,
                y: (from.y + (to.y - from.y) / 2)
            }
        });

        lines.push({
            start: {
                x: from.x,
                y: (from.y + (to.y - from.y) / 2)
            },
            end: {
                x: to.x,
                y: (from.y + (to.y - from.y) / 2)
            }
        });

        lines.push({
            start: {
                x: to.x,
                y: (from.y + (to.y - from.y) / 2)
            },
            end: {
                x: to.x,
                y: to.y
            }
        });

        return lines;
    }

    componentWillMount() {
        this.lines = this.getPath();
    }

    componentWillUpdate() {
        this.lines = this.getPath();
    }

    render() {

        return <div>
            {this.lines.map((line, index) => <Line key={index} className={this.props.className} style={this.props.style}
            from={line.start}
            to={line.end}/>)}
        </div>;
    }
}