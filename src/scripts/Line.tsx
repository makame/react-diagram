import * as React from 'react';
import * as ReactDOM from 'react-dom';

export class Line extends React.Component<{
    from: {
        x: number
        y: number
    }
    to: {
        x: number
        y: number
    }
    style?: string
    className?: string
}, {}> {

    render() {
        let from = this.props.from;
        let to = this.props.to;

        if (to.x < from.x) {
            from = this.props.to;
            to = this.props.from;
        }

        const len = Math.sqrt(Math.pow(from.x - to.x, 2) + Math.pow(from.y - to.y, 2));
        const angle = Math.atan((to.y - from.y) / (to.x - from.x));

        return <div className={this.props.className} style={{
            position: 'absolute',
            pointerEvents: 'none',
            transform: `translate(${from.x - .5 * len * (1 - Math.cos(angle))}px, ${from.y + .5 * len * Math.sin(angle)}px) rotate(${angle}rad)`,
            width: `${len}px`,
            height: `${0}px`,
            borderBottom: this.props.style || '2px solid rgba(0, 0, 0, 0.7)'
        }}></div>;
    }
}