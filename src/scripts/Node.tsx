import * as React from 'react';
import * as ReactDOM from 'react-dom';
import Rnd from "react-rnd";

import { LinePath } from './LinePath';

export class NodeData {
    x: number
    y: number
    width: number
}

export class Node extends React.Component<{
    title: string
    selected?: boolean
    blockKey: string | number
    step: number
    onClick: (e) => void
    valueLink: {
        set: (value: NodeData) => void
        get: () => NodeData
    }
    portRegister: (ref, type: 'in' | 'out', blockKey: string | number, key: string | number) => void
    portStart: (e: React.DragEvent<HTMLDivElement>, type: 'in' | 'out', blockKey: string | number, key: string | number) => void
    portEnd: (e: React.DragEvent<HTMLDivElement>, type: 'in' | 'out', blockKey: string | number, key: string | number) => void
    portOver: (e: React.DragEvent<HTMLDivElement>, type: 'in' | 'out', blockKey: string | number, key: string | number) => void
    portLeave: (e: React.DragEvent<HTMLDivElement>, type: 'in' | 'out', blockKey: string | number, key: string | number) => void
    portDrop: (e: React.DragEvent<HTMLDivElement>, type: 'in' | 'out', blockKey: string | number, key: string | number) => void
    ports: {
        key: string | number
        in?: {
            key: string | number
            name: string
            type?: 'std' | 'alt'
        },
        out?: {
            key: string | number
            name: string
            type?: 'std' | 'alt'
        }
    }[]
    type?: 'entry' | 'green' | 'structure' | 'logic' | 'error' | 'timer' | 'code'
}, {}> {

    rnd: Rnd

    componentWillUpdate() {
        if (this.rnd) {
            this.rnd.updatePosition({
                x: this.props.valueLink.get().x,
                y: this.props.valueLink.get().y
            });
            this.rnd.updateSize({
                width: this.props.valueLink.get().width
            })
        }
    }

    render() {
        let ports = this.props.ports.map(port => {
            return (
                <div className="ports" key={port.key}>
                    <div className="port">
                        {port.in && (
                            <div className={"in-port" + (port.in.type ? (' ' + port.in.type) : '')}>
                                <div key={port.in.key} className="contact" ref={ref => this.props.portRegister(ref, 'in', this.props.blockKey, port.in.key)} draggable={true} onDragStart={e => this.props.portStart(e, 'in', this.props.blockKey, port.in.key)} onDragEnd={e => this.props.portEnd(e, 'in', this.props.blockKey, port.in.key)} onDragOver={e => this.props.portOver(e, 'in', this.props.blockKey, port.in.key)} onDrop={e => this.props.portDrop(e, 'in', this.props.blockKey, port.in.key)} onDragLeave={e => this.props.portLeave(e, 'in', this.props.blockKey, port.in.key)}></div>
                                <div className="name">{port.in.name}</div>
                            </div>
                        )}
                    </div>
                    <div className="port">
                        {port.out && (
                            <div key={port.out.key} className={"out-port" + (port.out.type ? (' ' + port.out.type) : '')}>
                                <div className="name">{port.out.name}</div>
                                <div className="contact" ref={ref => this.props.portRegister(ref, 'out', this.props.blockKey, port.out.key)} draggable={true} onDragStart={e => this.props.portStart(e, 'out', this.props.blockKey, port.out.key)} onDragEnd={e => this.props.portEnd(e, 'out', this.props.blockKey, port.out.key)} onDragOver={e => this.props.portOver(e, 'out', this.props.blockKey, port.out.key)} onDrop={e => this.props.portDrop(e, 'out', this.props.blockKey, port.out.key)} onDragLeave={e => this.props.portLeave(e, 'out', this.props.blockKey, port.out.key)}></div>
                            </div>
                        )}
                    </div>
                </div>
            )
        })

        return (
            <Rnd ref={c => this.rnd = c} extendsProps={{ onClick: e => this.props.onClick(e) }} className={"node" + (this.props.type ? (' ' + this.props.type) : '') + (this.props.selected ? ' active' : '')} dragHandlerClassName=".title" bounds={".blueprint-container"} default={{
                x: this.props.valueLink.get().x,
                y: this.props.valueLink.get().y,
                width: this.props.valueLink.get().width,
                height: 'auto'
            }}
            onResizeStop={(e, direction, elem) => {
                this.props.valueLink.set({
                    x: this.props.valueLink.get().x,
                    y: this.props.valueLink.get().y,
                    width: $(elem).outerWidth()
                })
            }}
            onDrag={(e, data) => {
                this.props.valueLink.set({
                    x: data.x,
                    y: data.y,
                    width: this.props.valueLink.get().width
                })
            }}
            onDragStop={(e, data) => {
                this.props.valueLink.set({
                    x: data.x,
                    y: data.y,
                    width: this.props.valueLink.get().width
                })
            }}
            enableResizing={{
                right: true
            }}
            minWidth={this.props.step * 5}>
                <div className="title">
                    <div className="name">{this.props.title}</div>
                </div>
                {ports}
            </Rnd>
        );
    }
}