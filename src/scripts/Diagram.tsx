import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { LinePath } from './LinePath';
import { Node, NodeData } from './Node';
export * from './Node';

export class Port {
    blockKey: string | number
    key: string | number
    ref: any
    element?: JQuery
    type: 'in' | 'out'
}

export class PortNode {
    key: string | number
    in?: {
        key: string | number
        name: string
        disabled?: boolean
        type?: 'std' | 'alt'
    }
    out?: {
        key: string | number
        name: string
        disabled?: boolean
        type?: 'std' | 'alt'
    }
}

export class DiagramNode {
    key: string | number
    title: string
    valueLink: {
        set: (value: NodeData) => void
        get: () => NodeData
    }
    ports: PortNode[]
    type?: 'entry' | 'green' | 'structure' | 'logic' | 'error' | 'timer' | 'code'
}

export class DiagramConnection {
    key: string
    in: {
        blockKey: string | number
        key: string | number
    }
    out: {
        blockKey: string | number
        key: string | number
    }
    type?: 'subline'
}

export class Diagram extends React.Component<{
    connections: DiagramConnection[]
    height: {
        set: (value: number) => void
        get: () => number
    }
    connectionIs: (out: {
        blockKey: string | number, key: string | number
    }, _in: {
        blockKey: string | number, key: string | number
    }) => boolean
    connectionAdd: (out: {
        blockKey: string | number, key: string | number
    }, _in: {
        blockKey: string | number, key: string | number
    }) => void
    onSelect: (nodes: (string | number)[], connection: (string | number)[]) => void
    onRemove: (nodes: (string | number)[], connection: (string | number)[]) => void
    onDrop: (position: {
        x: number
        y: number
    }) => void
    onDropIs: (position: {
        x: number
        y: number
    }) => boolean
    nodes: DiagramNode[]
}, {}> {

    meterWidth: number = 1000
    width: number = 1000
    step: number = 10
    rate: number = 1

    removeEvent: any

    componentWillMount() {
        this.removeEvent = (e) => {
            this.removeSelected(e);
        };

        if (process.env.SERVER == "false") document.addEventListener("keydown", this.removeEvent, false);
    }

    componentWillUnmount() {
        if (process.env.SERVER == "false") document.removeEventListener("keydown", this.removeEvent, false);
    }

    removeSelected(e) {
        if ($(e.target)[0] === $('body')[0] && (e.keyCode == 46 || e.keyCode == 8)) {
            this.props.onRemove(this.nodes.filter(node => node.selected).map(node => node.key), this.connections.filter(connection => connection.selected).map(connection => connection.key));
        }
    }

    componentDidMount() {
        this.update();
    }

    componentDidUpdate() {
        this.update();
    }

    update() {
        this.blueprint = $(ReactDOM.findDOMNode(this.refs.blueprint));

        let updatePorts = this.updatePorts();
        let updateNodes = this.updateNodes();
        let updateConnections = this.updateConnections();
        let updateBlueprint = this.updateBlueprint();

        if (updatePorts && updateConnections && updateNodes && updateBlueprint) this.forceUpdate();

        let updated = true;

        if (updated) this.props.nodes.forEach(node => {
            if (updated && !(this.nodes.find(c => c.key == node.key))) updated = false;
        });
        if (updated) {
            let checkNext = false;

            this.props.connections.forEach(connection => {
                if (!checkNext && !(this.connections.find(c => c.key == connection.key))) checkNext = true;
            });

            if (checkNext) this.props.connections.filter(connection => !!this.props.nodes.find(n => n.key == connection.in.blockKey && !!n.ports.find(p => p.in && p.in.key == connection.in.key)) && !!this.props.nodes.find(n => n.key == connection.out.blockKey && !!n.ports.find(p => p.out && p.out.key == connection.out.key))).forEach(connection => {
                if (updated && !(this.connections.find(c => c.key == connection.key))) updated = false;
            });
        }

        if (updated && !(this.blueprint && this.blueprint.length)) updated = false;
        if (updated) this.nodes.forEach(node => {
            if (updated && !(node.element && node.element.length)) updated = false;
        });
        if (updated) this.ports.forEach(port => {
            if (updated && !(port.element && port.element.length)) updated = false;
        });
        if (updated) this.connections.forEach(connection => {
            if (updated && !(connection.element && connection.element.length)) updated = false;
        });

        if (!updated) this.forceUpdate();
    }

    updateBlueprint(): boolean {
        let updateBlueprint = false;

        if (this.blueprint && this.blueprint.length) {
            this.width = this.blueprint.outerWidth();
            let rate = this.width / this.meterWidth;

            if (rate != this.rate) {
                this.rate = rate;
                updateBlueprint = true;
            }
            else {
                this.rate = rate;
            }
        }

        return updateBlueprint;
    }

    updatePorts(): boolean {
        let added = false;
        this.ports.forEach(port => {
            let element = $(ReactDOM.findDOMNode(port.ref));
            if (!port.element && element) added = true;
            port.element = element;
        });
        return added;
    }

    getPort(type: 'in' | 'out', blockKey: string | number, key: string | number): any {
        return this.ports.find(port => port.blockKey == blockKey && port.key == key && port.type == type);
    }

    getPortElement(type: 'in' | 'out', blockKey: string | number, key: string | number): any {
        let find = this.getPort(type, blockKey, key);
        return find && find.element;
    }

    registerPortRef(ref, type: 'in' | 'out', blockKey: string | number, key: string | number) {
        let find = this.getPort(type, blockKey, key);
        if (find) {
            find.ref = ref;
        }
        else {
            this.ports.push({ blockKey, key, ref, type });
        }
    }

    updateNodes(): boolean {
        let added = false;
        this.nodes.forEach(node => {
            let element = $(ReactDOM.findDOMNode(node.ref));
            if (!node.element && element) added = true;
            node.element = element;
        });
        return added;
    }

    getNode(key: string | number): any {
        return this.nodes.find(node => node.key == key);
    }

    getConnection(key: string | number): {
        ref: LinePath
        key: string | number
        selected?: boolean
    } {
        return this.connections.find(connection => connection.key == key);
    }

    updateConnections(): boolean {
        let added = false;
        this.connections.forEach(connection => {
            let element = $(ReactDOM.findDOMNode(connection.ref));
            if (!connection.element && element) added = true;
            connection.element = element;
        });
        return added;
    }

    registerNodeRef(ref, key: string | number) {
        let find = this.getNode(key);
        if (find) {
            find.ref = ref;
        }
        else {
            this.nodes.push({ key, ref });
        }
    }

    registerConnectionRef(ref, key: string | number, inBlockKey: string | number, inKey: string | number, outBlockKey: string | number, outKey: string | number) {
        let find = this.getConnection(key);
        if (find) {
            find.ref = ref;
        }
        else {
            this.connections.push({ key, ref, inBlockKey, inKey, outBlockKey, outKey });
        }
    }

    updateSelection() {
        if (this.selectStart && this.selectEnd) {
            this.nodesInRect(this.selectStart, this.selectEnd, (node, value) => {
                node.selected = value;
            });
            this.connections.forEach(connection => {
                connection.selected = this.blueprint && this.blueprint.length ? connection.ref.crossedRect({
                    x: this.selectStart.x - this.blueprint.offset().left,
                    y: this.selectStart.y - this.blueprint.offset().top
                }, {
                    x: this.selectEnd.x - this.blueprint.offset().left,
                    y: this.selectEnd.y - this.blueprint.offset().top
                }) : false;
            });
        }
    }

    cleanSelection() {
        this.nodes.forEach(node => {
            node.selected = false;
        });
        this.connections.forEach(connection => {
            connection.selected = false;
        });
    }

    componentWillUpdate() {
        this.nodes = this.nodes.filter(node => !!this.props.nodes.find(n => n.key == node.key));
        this.ports = this.ports.filter(port => !!this.props.nodes.find(n => n.key == port.blockKey && !!n.ports.find(p => port.type == "in" && p.in && p.in.key == port.key || port.type == "out" && p.out && p.out.key == port.key)));
        this.connections = this.connections.filter(connection => !!this.props.connections.find(c => c.key == connection.key) && !!this.props.nodes.find(n => n.key == connection.inBlockKey && !!n.ports.find(p => p.in && p.in.key == connection.inKey)) && !!this.props.nodes.find(n => n.key == connection.outBlockKey && !!n.ports.find(p => p.out && p.out.key == connection.outKey)));
        this.updateSelection();
        if (!this.selectStart && !this.selectEnd) this.checkOnSelect();
    }

    selectedNodeState: (string | number)[]
    selectedConnectionState: (string | number)[]

    checkOnSelect() {
        let newNodeState = this.nodes.filter(node => node.selected).map(node => node.key);
        let newConnectionState = this.connections.filter(connection => connection.selected).map(connection => connection.key);

        if (JSON.stringify(newNodeState) != JSON.stringify(this.selectedNodeState) || JSON.stringify(newConnectionState) != JSON.stringify(this.selectedConnectionState)) this.props.onSelect(newNodeState, newConnectionState);

        this.selectedNodeState = newNodeState;
        this.selectedConnectionState = newConnectionState;
    }

    pointsInRect(points: {
        x: number
        y: number
    }[], start: {
        x: number
        y: number
    }, end: {
        x: number
        y: number
    }): boolean {
        let startX = end.x > start.x ? start.x : end.x;
        let startY = end.y > start.y ? start.y : end.y;
        let endX = end.x > start.x ? end.x : start.x;
        let endY = end.y > start.y ? end.y : start.y;

        let contains = false;
        points.forEach(point => {
            if (point.x >= startX && point.x <= endX && point.y >= startY && point.y <= endY) contains = true;
        });
        return contains;
    }

    nodesInRect(start: {
        x: number
        y: number
    }, end: {
        x: number
        y: number
    }, callback?: (node: {
        ref: any
        key: string | number
        element?: JQuery
        selected?: boolean
    }, value: boolean) => void) {
        let startX = end.x > start.x ? start.x : end.x;
        let startY = end.y > start.y ? start.y : end.y;
        let endX = end.x > start.x ? end.x : start.x;
        let endY = end.y > start.y ? end.y : start.y;
        return this.nodes.filter(node => {
            const value = node.element && node.element.length && ((node.element.offset().left >= startX && node.element.offset().left <= endX && node.element.offset().top >= startY && node.element.offset().top <= endY) || (node.element.offset().left + node.element.outerWidth() >= startX && node.element.offset().left + node.element.outerWidth() <= endX && node.element.offset().top >= startY && node.element.offset().top <= endY)|| (node.element.offset().left >= startX && node.element.offset().left <= endX && node.element.offset().top + node.element.outerHeight() >= startY && node.element.offset().top + node.element.outerHeight() <= endY)|| (node.element.offset().left + node.element.outerWidth() >= startX && node.element.offset().left + node.element.outerWidth() <= endX && node.element.offset().top + node.element.outerHeight() >= startY && node.element.offset().top + node.element.outerHeight() <= endY));
            if (callback) callback(node, value);
            return value;
        })
    }

    ports: Port[] = []
    dropped: Port
    droppedTo: Port

    nodes: {
        ref: Node
        key: string | number
        element?: JQuery
        selected?: boolean
    }[] = []

    connections: {
        ref: LinePath
        key: string | number
        element?: JQuery
        selected?: boolean
        inBlockKey: string | number
        inKey: string | number
        outBlockKey: string | number
        outKey: string | number
    }[] = []

    blueprintRef: any
    blueprint: JQuery
    selectStart: {
        x: number
        y: number
    }
    selectEnd: {
        x: number
        y: number
    }

    render() {
        let selection = null;
        if (this.blueprint && this.blueprint.length && this.selectStart && this.selectEnd) {
            let x = 0;
            let y = 0;
            let width = 0;
            let height = 0;

            if (this.selectEnd.x - this.selectStart.x < 0) {
                x = this.selectEnd.x;
                width = this.selectStart.x - this.selectEnd.x;
            }
            else {
                x = this.selectStart.x;
                width = this.selectEnd.x - this.selectStart.x;
            }

            if (this.selectEnd.y - this.selectStart.y < 0) {
                y = this.selectEnd.y;
                height = this.selectStart.y - this.selectEnd.y;
            }
            else {
                y = this.selectStart.y;
                height = this.selectEnd.y - this.selectStart.y;
            }

            selection = <div className="selection" style={{ left: x - this.blueprint.offset().left, top: y - this.blueprint.offset().top, width, height }}></div>
        }

        let dragLine = null;
        if (this.blueprint && this.blueprint.length && this.dropped && this.dropped.element.length && this.droppedTo && this.droppedTo.element.length) {
            let fromX = this.dropped.element.offset().left - this.dropped.element.closest('.blueprint-container').offset().left + this.dropped.element.outerWidth() / 2;
            let fromY = this.dropped.element.offset().top - this.dropped.element.closest('.blueprint-container').offset().top + this.dropped.element.outerHeight() / 2;
            let toX = this.droppedTo.element.offset().left - this.droppedTo.element.closest('.blueprint-container').offset().left + this.droppedTo.element.outerWidth() / 2;
            let toY = this.droppedTo.element.offset().top - this.droppedTo.element.closest('.blueprint-container').offset().top + this.droppedTo.element.outerHeight() / 2;

            dragLine = <LinePath
            from={{x: fromX, y: fromY, offset: 20, side: this.dropped.type}}
            to={{x: toX, y: toY, offset: 20, side: this.droppedTo.type}}/>
        }

        let connections = null;
        if (this.blueprint && this.blueprint.length) connections = this.props.connections.map(connection => {
            let link = this.getConnection(connection.key);

            let outPort = this.getPortElement('out', connection.out.blockKey, connection.out.key);
            let inPort = this.getPortElement('in', connection.in.blockKey, connection.in.key);

            if (outPort && inPort && outPort.length && inPort.length) {

                let fromX = outPort.offset().left - outPort.closest('.blueprint-container').offset().left + outPort.outerWidth() / 2;
                let fromY = outPort.offset().top - outPort.closest('.blueprint-container').offset().top + outPort.outerHeight() / 2;
                let toX = inPort.offset().left - inPort.closest('.blueprint-container').offset().left + inPort.outerWidth() / 2;
                let toY = inPort.offset().top - inPort.closest('.blueprint-container').offset().top + inPort.outerHeight() / 2;

                let style: string = null
                if (connection.type == 'subline') style = '2px solid rgba(0, 0, 0, 0.4)';
                if (link && link.selected) style = '2px dashed rgba(0, 192, 255, 0.8)';

                return (
                    <LinePath ref={ref => this.registerConnectionRef(ref, connection.key, connection.in.blockKey, connection.in.key, connection.out.blockKey, connection.out.key)} key={connection.key} from={{x: fromX, y: fromY, offset: 20, side: 'out'}} to={{x: toX, y: toY, offset: 20, side: 'in'}} style={style}/>
                )
            }
        })

        let nodes = null;
        if (this.blueprint && this.blueprint.length) nodes = this.props.nodes.map(node => {
            let block = this.getNode(node.key);
            return (
                <Node onClick={e => {
                    this.cleanSelection();
                    block.selected = true;
                    this.forceUpdate();
                }} ref={ref => this.registerNodeRef(ref, node.key)} selected={block && block.selected} key={node.key} title={node.title} type={node.type} blockKey={node.key} step={this.step * this.rate} valueLink={{
                    set: value => {
                        if (!Number.isNaN(value.x) && !Number.isNaN(value.y) && !Number.isNaN(value.width)) {
                            node.valueLink.set({
                                x: value.x / this.rate,
                                y: value.y / this.rate,
                                width: value.width / this.rate
                            })
                        }
                    },
                    get: () => ({
                        x: node.valueLink.get().x * this.rate,
                        y: node.valueLink.get().y * this.rate,
                        width: node.valueLink.get().width * this.rate
                    })
                }}
                portStart={(e, type, blockKey, key) => {
                    this.dropped = this.getPort(type, blockKey, key);
                    this.forceUpdate();
                }}
                portEnd={(e, type, blockKey, key) => {
                    if (this.dropped || this.droppedTo) {
                        this.dropped = null;
                        this.droppedTo = null;
                        this.forceUpdate();
                    }
                }}
                portOver={(e, type, blockKey, key) => {
                    let current = this.getPort(type, blockKey, key);
                    this.droppedTo = current;
                    if (this.dropped && this.dropped.type != current.type) {
                        if ((this.dropped.type == 'out' && this.props.connectionIs({
                                blockKey: this.dropped.blockKey,
                                key: this.dropped.key
                            }, {
                                blockKey: current.blockKey,
                                key: current.key
                            })) || this.props.connectionIs({
                                blockKey: current.blockKey,
                                key: current.key
                            }, {
                                blockKey: this.dropped.blockKey,
                                key: this.dropped.key
                            })) {
                            e.preventDefault();
                        }
                        this.forceUpdate();
                    }
                }}
                portLeave={(e, type, blockKey, key) => {
                    if (this.droppedTo) {
                        this.droppedTo = null;
                        this.forceUpdate();
                    }
                }}
                portDrop={(e, type, blockKey, key) => {
                    let current = this.getPort(type, blockKey, key);
                    if (this.dropped.type != current.type) {
                        if (this.dropped.type == 'out') {
                            this.props.connectionAdd({
                                blockKey: this.dropped.blockKey,
                                key: this.dropped.key
                            }, {
                                blockKey: current.blockKey,
                                key: current.key
                            })
                        }
                        else {
                            this.props.connectionAdd({
                                blockKey: current.blockKey,
                                key: current.key
                            }, {
                                blockKey: this.dropped.blockKey,
                                key: this.dropped.key
                            })
                        }
                    }
                    this.dropped = null;
                    this.droppedTo = null;
                    this.forceUpdate();
                }}
                portRegister={this.registerPortRef.bind(this)}
                ports={node.ports}/>
            )
        })

        return (
            <div ref="blueprint" key="blueprint-container" className="blueprint-container" style={{
                height: this.props.height.get() * this.rate,
                minHeight: this.props.height.get() * this.rate,
                maxHeight: this.props.height.get() * this.rate,
                width: '100%'
            }}>
                <div key="blueprint" ref={ref => this.blueprintRef = ref} className="blueprint" style={{
                    height: this.props.height.get() * this.rate,
                    minHeight: this.props.height.get() * this.rate,
                    maxHeight: this.props.height.get() * this.rate,
                    width: '100%'
                }} onMouseLeave={e => {
                    this.selectStart = null
                    this.selectEnd = null
                    this.forceUpdate();
                }} onMouseMove={e => {
                    if (this.selectStart) {
                        this.selectEnd = {
                            x: e.pageX,
                            y: e.pageY
                        }
                        this.forceUpdate();
                    }
                }} onMouseUp={e => {
                    if (this.selectStart || this.selectEnd) {
                        this.selectStart = null
                        this.selectEnd = null
                        this.forceUpdate();
                    }
                }} onDragOver={e => {
                    if (!this.dropped && !this.droppedTo && this.blueprint && this.blueprint.length && this.props.onDropIs({
                        x: (e.pageX - this.blueprint.offset().left) / this.rate,
                        y: (e.pageY - this.blueprint.offset().top) / this.rate,
                    })) e.preventDefault();
                    }}>
                    <div className="selector" onMouseDown={e => {
                        this.cleanSelection();
                        this.selectStart = {
                            x: e.pageX,
                            y: e.pageY
                        }
                        this.forceUpdate();
                    }} onDrop={e => {
                        if (!this.dropped && !this.droppedTo && this.blueprint && this.blueprint.length && this.props.onDropIs({
                        x: (e.pageX - this.blueprint.offset().left) / this.rate,
                        y: (e.pageY - this.blueprint.offset().top) / this.rate,
                    })) this.props.onDrop({
                        x: (e.pageX - this.blueprint.offset().left) / this.rate,
                        y: (e.pageY - this.blueprint.offset().top) / this.rate,
                    });
                    }}/>
                    {dragLine}
                    {selection}
                    {connections}
                    {nodes}
                </div>
            </div>
        )
    }
}